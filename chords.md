# Chords

A chord is typically three or more different notes sounding at the same time.


## Chord Notation
Chord tones and extensions are notated relative to the major scale.

The intervals ```2, 4, 6``` are usually notated an octave above as ```7, 9, 13```. But this doesn't means they have to be an octave above necessarily.

## Triads
The most basic chord. A triad is constructed by stacking 3rds on top of each other.

Since there are 2 kinds of 3rds intervals (major 3rd, minor 3rd), there are 4 different triads from these combinations:
- Major
- Minor
- Diminished
- Augmented

They follow the "1 3 5" intervallic pattern with some modifications.

## 7th Chords
Take the pattern established from the constructing a triad, stack another 3rd on top of it, and now you've got 7th chords!!

### Commonly Used 7th Chords
#### Major 7th
#### Dominant 7th
#### Minor 7th
#### Minor 7 flat 5
#### Diminished 7th
Also called the "fully diminished 7" to distinguish it from the previous chord.

### Less Commonly Used 7th Chords
#### Major Minor 7th
The ['James Bond'](https://www.youtube.com/watch?v=U9FzgsF2T-s) chord. At around 0:17, where the brass plays the main theme, which is an arpeggiated Major Minor 7th chord.

Contains an augmented triad consisting of the b3, 5, and 7.
#### Augmented Major 7th
#### Augmented Dominant 7th
This chord doesn't follow the formula of adding 3rds.
#### Diminished Major 7th
|1|&#9837;3|&#9837;5|7|
|--|--|--|--|
|C|E&#9837;|G&#9837;|B

This chord also doesn't follow the formula of adding another ```3rd``` on top of the triad (the distance between the ```b5``` and the ```7``` is actually a ```4th```), but we'll put it here for completion's sake.

Also, because of that ```4th``` interval, this chord can be re-written as an inversion:

> E.g. C dim Maj 7 = C Eb Gb B = B / C = C D# F# B


## Chord Extensions
These are chords that add on additional notes other than the 'basic' ones that define the chord.

### 'Add' Chords
Cadd9

### Suspended Chords

### Extensions without 7
When a chord is notated with a number < 7, it means the 7th is _not_ included in that chord.

> Dm6 = D F A B

### Extensions with 7
When a chord is notated with a number > 7, it means the 7th _is_ included in that chord.

> Dm13 = D F A C B

Some people consider extensions to include all other extensions before them, (e.g. Dm13 contains 9 and 11), but I use them more as guidelines. As in if Dm13 is specified, 9 and 11 are optional but you can put them in depending on the context / feel you are going for.

### Extensions with Multiple Numbers
Some chords may be written with multiple numbers like this: D69 or Dm69. This means you take both of the extensions, but ignore the 7th:

D69
D F# A B E
Dm69
D F A B E

### Altered Extensions
This is how you get super spicy chords!!!

## Voice Leading
When you get to stringing a bunch of chords together, especially more complicated ones, the order and octave of notes actually starts to matter.

**Voice Leading** is how you arrange the notes within the chord (how close, which octave), and how these arrangements vary over time (e.g. closely spaced to widely spaced).