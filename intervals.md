# Intervals
An interval is the space between two notes.

In relative notation, this is notated using the spacings from the major scale:

> E.g. a b6 would be...
> - a 6 from the major scale, then 'flatted', or down 1 half step.  




