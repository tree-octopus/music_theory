# Scales
A scale can be defined as an ordered set of musical notes with a specific set of [intervals][int] between them.

Each scale has its own characteristic sound, courtesy of not only the intervals between its notes, but also the chords and chord progressions that can be formed by using those notes.

## Modes
A **mode** is when you take a scale and build another scale starting from a scale degree other than the 1.

Modes can be considered separate 'scales' in their own right.


[int]: ./intervals.md